# Install
docker build . -t fizzbuzz  
docker run -v ${PWD}:/usr/src/app -it --rm fizzbuzz composer install 

# Create spec
./bin/test desc "Acme\FizzBuzz" 

# Run tests 
./bin/test run --format=pretty 

# Requirements

```
Feature: FizzBuzz Game

  Scenario: Play FizzBuzz to get Fizz
    Given I play the FizzBuzz game
    When I play with a multiple of 3
    Then The result should be "Fizz"

  Scenario: Play FizzBuzz to get Buzz
    Given I play the FizzBuzz game
    When I play with a multiple of 5
    Then The result should be "Buzz"

  Scenario: Play FizzBuzz to get FizzBuzz
    Given I play the FizzBuzz game
    When I play with multiples of both 3 and 5
    Then The result should be "FizzBuzz"
```